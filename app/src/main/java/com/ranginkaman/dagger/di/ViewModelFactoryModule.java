package com.ranginkaman.dagger.di;

import androidx.lifecycle.ViewModelProvider;

import com.ranginkaman.dagger.viwemodels.ViewModelProviderFactory;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelFactoryModule {

    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory modelProviderFactory);

}
