package com.ranginkaman.dagger.di;

import com.ranginkaman.dagger.di.auth.AuthModule;
import com.ranginkaman.dagger.di.auth.AuthScope;
import com.ranginkaman.dagger.di.auth.AuthViewModelsModule;
import com.ranginkaman.dagger.di.main.MainFragmentBuildersModule;
import com.ranginkaman.dagger.di.main.MainModule;
import com.ranginkaman.dagger.di.main.MainScope;
import com.ranginkaman.dagger.di.main.MainViewModelModule;
import com.ranginkaman.dagger.ui.auth.AuthActivity;
import com.ranginkaman.dagger.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ActivityBuildersModule {

    @AuthScope
    @ContributesAndroidInjector(
            modules = {AuthViewModelsModule.class, AuthModule.class}
    )
    abstract AuthActivity contributeAuthActivity();

    @MainScope
    @ContributesAndroidInjector(
            modules = {MainFragmentBuildersModule.class, MainViewModelModule.class, MainModule.class}
    )
    abstract MainActivity contributeMainActivity();

}
