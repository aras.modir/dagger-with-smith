package com.ranginkaman.dagger;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import com.ranginkaman.dagger.models.User;
import com.ranginkaman.dagger.ui.auth.AuthResource;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SessionManager {
    private static final String TAG = "Test";

    private MediatorLiveData<AuthResource<User>> cashedUser = new MediatorLiveData<>();

    @Inject
    public SessionManager() {

    }

    public void authenticateWithId(final LiveData<AuthResource<User>> source) {
        if (cashedUser != null) {
            cashedUser.setValue(AuthResource.loading((User)null));
            cashedUser.addSource(source, new Observer<AuthResource<User>>() {
                @Override
                public void onChanged(AuthResource<User> userAuthResource) {
                    cashedUser.setValue(userAuthResource);
                    cashedUser.removeSource(source);
                }
            });
        } else {
            Log.d(TAG, "authenticateWithId: previous session detected. Retrieving user from cache.");
        }
    } 
    
    public void logOut() {
        Log.d(TAG, "logOut: logging out...");
        cashedUser.setValue(AuthResource.<User>logout());
    }

    public  LiveData<AuthResource<User>> getAuthUser() {
        return cashedUser;
    }

}
